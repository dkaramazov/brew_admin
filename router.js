(function(){
    'use strict';

    angular
    .module('app')
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");        

        var homeState = {
            name: 'home',
            url: '/',
            templateUrl: 'views/home.html'
          }

        var addState = {
          name: 'add',
          url: '/add',
          templateUrl: 'views/upsert.html'
        }

        var updateState = {
          name: 'update',
          url: '/update/:id',
          templateUrl: 'views/upsert.html',
          controller: function($scope, $stateParams){
            $scope.id = $stateParams.id;
          }
        }
      
        var stockState = {
            name: 'stock',
            url: '/stock',
            templateUrl: 'views/stock.html'
          }

        $stateProvider.state(homeState);
        $stateProvider.state(stockState);
        $stateProvider.state(addState);
        $stateProvider.state(updateState);
      });

})()