(function () {
    'use strict';
    
    angular
        .module('app')
        .controller('stockCtrl', stockCtrl);

    stockCtrl.$inject = ['$scope', '$state', '$stateParams', '$window', '$location', '$anchorScroll', 'appConfigSvc', 'stockSvc'];

    function stockCtrl($scope, $state, $stateParams, $window, $location, $anchorScroll, appConfigSvc, stockSvc) {
        var vm = this;

        vm.stocks = stockSvc.getAll().then(function (data) {
            vm.stocks = data;
        });
        vm.id = $stateParams.id;        
        vm.remove = remove;
        vm.updateStatus = updateStatus;

        function remove(id){
            for(var i = 0; i < vm.stocks.length; i++){
                if(vm.stocks[i].id === id){
                    vm.stocks.splice(i, 1);
                    break;
                }
            }
            function success(response){
                console.log('deleted', response);
            }
            stockSvc.remove(id).then(success).catch(error);
        }

        function updateStatus(stock){
            let _stock = {
                _id: stock._id,
            }
            function success(response){
                stock.isCompleted = !stock.isCompleted;
            }
            stockSvc.update(_stock).then(success).catch(error);
        }
        
    };

    // Error handling
    function error(error){
        console.log(error);
    }

})();