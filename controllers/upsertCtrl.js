(function () {
    'use strict';
    
    angular
        .module('app')
        .controller('upsertCtrl', upsertCtrl);

    upsertCtrl.$inject = ['$scope', '$state', '$stateParams', '$window', '$location', '$anchorScroll', 'appConfigSvc', 'stockSvc'];

    function upsertCtrl($scope, $state, $stateParams, $window, $location, $anchorScroll, appConfigSvc, stockSvc) {
        var vm = this;
        
        vm.stock = stockSvc.getById($stateParams.id).then(function (data) {
            vm.isEditing = data && data.id ? true:false;
            vm.stock = data;
        });
        vm.isEditing = false;
        vm.upsert = upsert;
        vm.editing = editing;

        function upsert(item){
            let result;

            function success(response){
                var obj = response.data;
                var update = false;
                for(var i = 0; i < vm.stocks.length; i++){
                    if(vm.stocks[i].id === obj.id){
                        update = true;
                        vm.stocks[i] = obj;
                        break;
                    }
                }
                if(!update){
                    vm.stocks.push(stock);  
                }
                $state.go('home');
            }

            if(item.id){
                let updateStockItem = {
                    id: item.id,
                    text: item.text,
                    current_count: item.current_count                
                };
                stockSvc.update(updateStockItem).then(success).catch(error);
            } else{
                let newStockItem = {
                    text: item.text,
                    par: item.par,
                    order_quantity: item.order_quantity,
                    location: item.location,
                    vendor: item.vendor
                };
                stockSvc.add(newStockItem).then(success).catch(error);
            }
        }

        function editing(stock, state){
            if(state){
                stock.isEditing = state;
            } else{
                delete stock.isEditing;
            }
        }        
        
    };

    // Error handling
    function error(error){
        console.log(error);
    }

})();