(function () {
    'use strict';

    angular
        .module('app', ['ui.router'])
        .constant('appConfigSvc', (appConfigSvc)())
        .factory('sessionInjector', ['appConfigSvc', function(appConfigSvc) {  
            var sessionInjector = {
                request: function(config) {
                    config.headers['x-api-key'] = appConfigSvc.Session.Key;
                    return config;
                }
            };
            return sessionInjector;
        }])
        .config(['$httpProvider', function($httpProvider) {  
            $httpProvider.interceptors.push('sessionInjector');
        }])
        .config(["$locationProvider", function($locationProvider){
            $locationProvider.html5Mode(true);
        }]);

    function appConfigSvc() {
        return {
            Session: {
                Submitted: false,
                ServiceUrl: 'https://inventory.baristavision.com/stock',
                Key: 'thaJ9YpMUS4BdCWMHLYVG6NuviA4cCUd5Q2cj28a'
            }
        }
    }

})();