(function () {
    'use strict';

    angular
        .module('app')
        .factory('stockSvc', stockSvc);

    function stockSvc($http, $q, appConfigSvc) {

        var service = {
            getAll: _getAll,
            getById: _getById,
            add: add,
            remove: remove,
            update: update
        };

        return service;

        function add(stock){
            return $http.post(appConfigSvc.Session.ServiceUrl, JSON.stringify(stock));
        }

        function remove(id){
            return $http.delete(appConfigSvc.Session.ServiceUrl + '/'+ id);
        }

        function update(stock){
            return $http.put(appConfigSvc.Session.ServiceUrl + '/' + stock.id, JSON.stringify(stock));
        }

        function _getById(id){
            function success(response) {
                return response.data;
            };
            function error(error) {
                console.log("Error: " + error);
            };
            var req = {
                method: 'GET',
                url: appConfigSvc.Session.ServiceUrl + '/' + id
            }
            return $http(req)
                .then(success, error)
                .catch(error);
        }

        function _getAll() {
            function success(response) {
                return response.data;
            };
            function error(error) {
                console.log("Error: " + error);
            };
            var req = {
                method: 'GET',
                url: appConfigSvc.Session.ServiceUrl
            }
            return $http(req)
                .then(success, error)
                .catch(error);
        }
        
    };

})();
